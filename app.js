"use strict";
const ul = document.getElementById("resultat");
const ville = document.getElementById("ville");
const carburant = document.querySelector(".carburant");
const bouton = document.getElementById("btn");
const form = document.getElementsByTagName("form");

bouton.addEventListener("click", (e) => {
  e.preventDefault();
  displayData();
});

// Reset le champ texte de mon input à chaque click
ville.addEventListener("click", () => (ville.value = ""));

function createNode(element) {
  return document.createElement(element);
}

function append(parent, el) {
  return parent.appendChild(el);
}

function displayData() {
  // Clear la liste à chaque click sur le bouton "rechercher"
  ul.innerHTML = "";

  fetch(
    `https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/prix_des_carburants_j_7/records?limit=20&refine=city%3A%22${ville.value.toUpperCase()}%22&refine=fuel%3A%22${
      carburant.value
    }%22`
  )
    .then((response) => response.json())
    .then((data) => {
      let stations = data.results;
      // console.log(stations);
      //Identifer le prix le plus bas grâce à une boucle me permettant de mettre toutes les valeurs de prix par carburant dans un tableau
      let tabPrix = [];
      for (let i = 0; i < stations.length; i++) {
        switch (carburant.value) {
          case "E10":
            tabPrix[i] = stations[i].price_e10 * 1000;
            break;
          case "E85":
            tabPrix[i] = stations[i].price_e85 * 1000;
            break;
          case "SP95":
            tabPrix[i] = stations[i].price_sp95 * 1000;
            break;
          case "SP98":
            tabPrix[i] = stations[i].price_sp98 * 1000;
            break;
          case "Gazole":
            tabPrix[i] = stations[i].price_gazole * 1000;
            break;
          case "GPLc":
            tabPrix[i] = stations[i].price_gplc * 1000;
        }
      }
      // Tri du tableau des prix
      tabPrix.sort();
      // Affectation de la valeur à l'index 0 (représantant mon prix le plus bas) dans une variable
      let prixBas = tabPrix[0];

      return stations.map(function (station) {
        // Création des éléments HTML pour affichage
        let li = createNode("li");
        let spanNom = createNode("span");
        let spanPrix = createNode("span");
        let spanAdresse = createNode("span");
        let spanAutomate = createNode("span");
        let spanNonRenseigne = createNode("span");
        // Passage par une variable "price" pour résoudre un bug d'affichage et fixé le prix à 3 chiffres après la virgule
        let price;
        // Switch permettant d'attribuer les valeurs à afficher à chaque span, selon le carburant choisi par l'utilisateur via le select
        switch (carburant.value) {
          case "E10":
            spanNom.innerHTML = `${station.name}`;
            price = (station.price_e10 * 1000).toFixed(3);
            spanPrix.innerHTML = `${price}`;
            spanAdresse.innerHTML = `${station.address}`;
            spanAutomate.innerHTML = `Automate 24/24 : ${station.automate_24_24}`;
            break;
          case "E85":
            spanNom.innerHTML = `${station.name} `;
            price = (station.price_e85 * 1000).toFixed(3);
            spanPrix.innerHTML = `${price}`;
            spanAdresse.innerHTML = `${station.address}`;
            spanAutomate.innerHTML = `Automate 24/24 : ${station.automate_24_24}`;
            break;
          case "SP95":
            spanNom.innerHTML = `${station.name}`;
            price = (station.price_sp95 * 1000).toFixed(3);
            spanPrix.innerHTML = `${price}`;
            spanAdresse.innerHTML = `${station.address}`;
            spanAutomate.innerHTML = `Automate 24/24 : ${station.automate_24_24}`;
            break;
          case "SP98":
            spanNom.innerHTML = `${station.name} `;
            price = (station.price_sp98 * 1000).toFixed(3);
            spanPrix.innerHTML = `${price}`;
            spanAdresse.innerHTML = `${station.address}`;
            spanAutomate.innerHTML = `Automate 24/24 : ${station.automate_24_24}`;
            break;
          case "Gazole":
            spanNom.innerHTML = `${station.name} `;
            price = (station.price_gazole * 1000).toFixed(3);
            spanPrix.innerHTML = `${price}`;
            spanAdresse.innerHTML = `${station.address}`;
            spanAutomate.innerHTML = `Automate 24/24 : ${station.automate_24_24}`;
            break;
          case "GPLc":
            spanNom.innerHTML = `${station.name}`;
            price = (station.price_gplc * 1000).toFixed(3);
            spanPrix.innerHTML = `${price}`;
            spanAdresse.innerHTML = `${station.address}`;
            spanAutomate.innerHTML = `Automate 24/24 : ${station.automate_24_24}`;
        }

        // Attribution de classes CSS aux span choisis
        spanPrix.setAttribute("class", "prix");
        spanNom.setAttribute("class", "nom");

        // Cas où station.name = null, remplacement de "null" par un texte choisi + attribution d'une classe CSS particulière
        if (station.name === null) {
          spanNom.setAttribute("class", "nonDefini");
          spanNom.innerHTML = "Nom non renseigné";
        }

        // Attribution d'une classe CSS permettant d'afficher le prix le plus bas en tête de la liste + animation
        if (price == prixBas) {
          li.setAttribute("class", "prixBas");
        }

        // Cas où station.automate_24_24 = null, remplacement de "null" par un texte choisi dans un nouveau span + attribution d'une classe CSS particulière à ce span
        if (station.automate_24_24 === null) {
          spanAutomate.innerHTML = "Automate 24/24 : ";
          spanNonRenseigne.innerHTML = "non renseigné";
          spanNonRenseigne.setAttribute("class", "nonDefini");
          append(spanAutomate, spanNonRenseigne);
        }

        // Ajout des éléments HTML créés à la liste "ul"
        append(ul, li);
        append(li, spanNom);
        append(li, spanAdresse);
        append(li, spanAutomate);
        append(li, spanPrix);
      });
    })
    .catch(function (error) {
      console.log(error);
    });
}
